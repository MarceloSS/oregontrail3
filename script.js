class Traveler {
    constructor (travelerName){
        this.name = travelerName;
        this.food = 1;
        this.hunger = 1;
        this.huntAbility = 2;
        this.isHealthy = true;
    }
    hunt(){
        this.food += this.huntAbility;
    }
    eat(){
        if(this.food > 0){
            this.food -= 1;
            this.isHealthy = true
        }else if(this.food === 0){
            this.isHealthy = false;
        }
    }
}

class Doctor extends Traveler {
    constructor (travelerName){
        super(travelerName);
    }
    heal (patient){
        if(!patient.isHealthy){
            patient.isHealthy = true
        }
    }
}

class Hunter extends Traveler {
    constructor (travelerName){
        super(travelerName)
        this.food = 2;
        this.hunger = 2;
        this.huntAbility = 5;
    }
    giveFood (traveler, quantity){
        if(this.food >= quantity){
            this.food -= quantity;
            traveler.food += quantity;
        }
    }
    eat (){
        if(this.food > 1){
            this.food -= 2;
            this.isHealthy = true
        } else if (this.food == 1){
            this.food -= 1;
            this.isHealthy = false
        } else if (this.food == 0){
            this.isHealthy = false
        }
    }
}
class Wagon {
    constructor (wagonCapacity){
        this.capacity = wagonCapacity
        this.passengers = []
    }
    getAvailableSeatCount(){
        let leftSpace = this.capacity - this.passengers.length
        return leftSpace;
    };
    
    join(traveler){
        if(this.getAvailableSeatCount() > 0){
            this.passengers.push(traveler)
        }
    };
    
    shouldQuarantine = function(){
    
       if(this.passengers.some(passenger => !passenger.isHealthy)) return true
    
       return false
    };
    
    totalFood = function(){
        let foodCount = 0
        this.passengers.forEach(passenger => {
            foodCount += passenger.food
        });
        return foodCount
    };
    
}

//teste

// Cria uma carroça que comporta 4 pessoas
let wagon = new Wagon(4);
// Cria cinco viajantes
let henrietta = new Traveler('Henrietta');
let juan = new Traveler('Juan');
let drsmith = new Doctor('Dr. Smith');
let sarahunter = new Hunter('Sara');
let maude = new Traveler('Maude');

console.log(`#1: There should be 4 available seats. Actual: ${wagon.getAvailableSeatCount()}`);

wagon.join(henrietta);
console.log(`#2: There should be 3 available seats. Actual: ${wagon.getAvailableSeatCount()}`);

wagon.join(juan);
wagon.join(drsmith);
wagon.join(sarahunter);

wagon.join(maude); // Não tem espaço para ela!
console.log(`#3: There should be 0 available seats. Actual: ${wagon.getAvailableSeatCount()}`);

console.log(`#4: There should be 5 total food. Actual: ${wagon.totalFood()}`);

sarahunter.hunt(); // pega mais 5 comidas

drsmith.hunt();
console.log(`#5: There should be 12 total food. Actual: ${wagon.totalFood()}`);

henrietta.eat();
sarahunter.eat();
drsmith.eat();
juan.eat();
juan.eat(); // juan agora está doente (sick)

console.log(`#6: Quarantine should be true. Actual: ${wagon.shouldQuarantine()}`);
console.log(`#7: There should be 7 total food. Actual: ${wagon.totalFood()}`);

drsmith.heal(juan);
console.log(`#8: Quarantine should be false. Actual: ${wagon.shouldQuarantine()}`);

sarahunter.giveFood(juan, 4);
sarahunter.eat(); // Ela só tem um, então ela come e fica doente

console.log(`#9: Quarantine should be true. Actual: ${wagon.shouldQuarantine()}`);
console.log(`#10: There should be 6 total food. Actual: ${wagon.totalFood()}`);